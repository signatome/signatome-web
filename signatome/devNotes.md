# Where am I?

- 2020/03/15

  Trying to get 'tenant' log in working on a home page. Once this is setup, the user can access the visitor sign in features.

- 2020/03/02

  I've decided that my priority should be to get the most basic version of my full stack application up and running before worrying about refactoring, UI changes, or considering Ionic.

  - [x] Add Firebase
  - [x] Get hosted 👍
  - [ ] Add basic auth
  - [ ] Add a database hook to actually read/write to the backend

- 2020/03/01

  Should I use Ionic ?

  | Feature | Native | PWA |
  | ------- | ------ | --- |
  | GPS     | T      | T?  |

- 2020/02/29

  I decided to use Firebase for the backend. I'm interested in configuring a firebase project in a scripted manner as much as possible. At this time, I don't have any intent to run Signatome as SAAS, however the thought is definitely on my mind. My main reason for not doing that is that I don't think I can make any money off of this in the near term and I don't want to be on the hook for a Firebase bill if my project gets any moderate amount of use. The current plan is to create as much of the app as I can as code and configurations within this git repository. Then someone can clone it and set up their own firebase account and project. That way I won't be on the hook for their bill.

  My other reason for going this route, at least initially, is that if this were a SAAS product, then I'd want it to be a multi-tenant application. I don't have enough experience with Firebase at this time to know how best to architect that sort of thing. Given that the data in the data base could potentially be a bit sensitive (detailed visitor history) I'm not sure how to securely manage that.

  Consider the kinds of queries that you want to support for managing the visitor log. This will help inform the database structure.

  Interested in PrimeNG

# Feature Ideas
