# Feature Ideas

Add feature ideas here

## WiFi sign in

Visitor's signed in and out when their device connects to the network.

### Description

If a visitor's WiFi device is associated with their visitor profile, then we can automatically sign them in (AND OUT!) when their device connects and disconnects from the guest WiFi. This would make signing in and out as simple as possible for the visitor. Sign out is especially valuable because visitor's frequently forget to do so.

This feature would require a cloud function to be triggered by a monitoring service connected to the guest WiFi. Configuring the software to call the cloud function via router monitors will probably be the hardest part of this.

Confirmation of successful sign in can be communicated to the user by email, sms, or even a local kiosk automated greeting.

## In app Kiosk registration

An admin should be able to use the website to configure a new Kiosk 'user' account. A kiosk user account represents the unique account associated with a physical location or device. Actually, that may not be the right way to go. location should probably be an attribute of a visit rather than being strictly tied to a particular kiosk user account

If a building has multiple sign in kiosks, do the kiosks need individual accounts? If we can grab kiosk device info right from the app/browser, then there wouldn't be any real need. What would happen if a kiosk gets signed out? would all kiosks sharing the common "kiosk user" account get signed out? There is also the question of remote setup.

## Directory import

From limited research, it appears that there are already Firebase integrations for most single sign on and user directory services such as OneLogin. Having the option to link an LDAP or other directory would be a great way to pre-populate Visit attributes such as the person a visitor is meeting with.

## Admin customizable sign in forms via Formly

Client admin could define their own forms via JSON to update the sign in form without requiring a rebuild of the site.

## Plugins

Create a suite of plugins that an admin can add to their Signatome distribution. Perhaps this could be a monitization route?

### QR Code sign ins

Visitors could be issued a QR code via email or on a temporary access badge that would contain their information.

#### use case

- User is issued QR Code
- User presents QR Code to camera in Signatome kiosk
- User is 'signed in'

## Face detection / identification

If a visitor's face is detected by the kiosk, this can be used to sign in the visitor. Their picture can also be used to enhance other features. For example, if the kiosk is capable of printing temp badges, the visitors face could be included. Their face could also be included in a notification email sent to the point of contact that they are visiting to allow their contact to easily identify them when meeting in a lobby.
