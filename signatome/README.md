# Signatome

Signatome is a web application to support the process of visitor sign in. It is intended for use in offices, events, or anywhere people need to sign in when visiting.

Signatome is ideal for sign in kiosks.

# Objectives

Signatome aims to do two things

1. Make the process of signing in as convenient as possible for visitors
2. Make the process of managing visitor access as convenient as possible
