// prettier.config.js or .prettierrc.js
module.exports = {
  trailingComma: "es5",
  singleQuote: true,
  printWidth: 120,
  useTabs: false,
  tabWidth: 2,
};
