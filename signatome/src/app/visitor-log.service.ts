import { Visitor } from './visitor-group/visitor/visitor.model';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
/**
 * This service tracks visitors
 */
@Injectable({ providedIn: 'root' })
export class VisitorLogService {
  // the data stored here will be moved to the backend once it exists
  // data = new Observable<Visitor[]>();
  data = new BehaviorSubject<Visitor[]>([new Visitor({ name: 'test', arrivalTime: new Date() })]);

  // currentVisitors$ = this.data;
  currentVisitors$: Observable<Visitor[]> = this.data.asObservable();

  hasVisitors$: Observable<boolean> = this.data.pipe(
    map((visitors: Visitor[]) => {
      return visitors && visitors.length > 0;
    }),
    share()
  );

  // private visitorsCollection: AngularFirestoreCollection<Visitor>;
  // constructor(private afs: AngularFirestore) {
  //   this.visitorsCollection = afs.collection<Visitor>('visitors');
  //   this.data = this.visitorsCollection.valueChanges();
  // }
  // addVisitor(item: Visitor) {
  //   this.visitorsCollection.add(item);
  //   this.visitorsCollection.add;
  // }

  // for debugging and early development
  signedOut: Visitor[] = [];

  // how to manage all visitors and current visitors. really don't need to maintain
  // the logged out visitors in state.

  signIn(visitors: Visitor[]) {
    console.log('sign in called');
    console.log(visitors);
    const withArrivalTime = this.timestampIn(visitors);
    // withArrivalTime.forEach(visitor => this.addVisitor(visitor));
    // this.data.next(this.data.getValue().concat(withArrivalTime));
  }

  signOutVisitor(name: string) {
    // const visitors = this.data.value.slice();
    // const toBeSignedOut = visitors.filter(visitor => visitor.name === name);
    // const toStaySignedIn = visitors.filter(visitor => visitor.name !== name);
    // this.signedOut.push(...this.timestampOut(toBeSignedOut));
    // this.data.next(toStaySignedIn);
  }

  signOutVisitors(names: string[]) {
    // const visitors = this.data.value.slice();
    // const toBeSignedOut = visitors.filter(visitor => names.includes(visitor.name));
    // const toStaySignedIn = visitors.filter(visitor => !names.includes(visitor.name));
    // console.log('toBeSignedOut: ', toBeSignedOut);
    // console.log('toStaySignedIn: ', toStaySignedIn);
    // this.signedOut.push(...this.timestampOut(toBeSignedOut));
    // this.data.next(toStaySignedIn);
  }

  signOutGroupByMember(name: string) {
    // const member = this.data.getValue().find(visitor => visitor.name === name);
    // const minute = member.arrivalTime.getMinutes();
    // const start = new Date(member.arrivalTime);
    // start.setMinutes(minute - 3);
    // const end = new Date(member.arrivalTime);
    // end.setMinutes(minute + 3);
    // this.signOutGroup({ dateTimeWindowStart: start, dateTimeWindowEnd: end });
  }

  signOutGroup({ dateTimeWindowStart, dateTimeWindowEnd }: { dateTimeWindowStart: Date; dateTimeWindowEnd: Date }) {
    // const start = dateTimeWindowStart.getTime();
    // const end = dateTimeWindowEnd.getTime();
    // const withDepartureTime = this.timestampOut(
    //   this.data.getValue().filter(visitor => {
    //     const dateTime = visitor.arrivalTime.getTime();
    //     return dateTime >= start && dateTime <= end;
    //   })
    // );
    // this.data.next(withDepartureTime);
  }

  private timestampIn(visitors: Visitor[]): Visitor[] {
    const retVal = [];
    const now = new Date();
    visitors.forEach(visitor => {
      // done by a backend service for complex things like organization affiliation
      retVal.push(new Visitor({ ...visitor, arrivalTime: now }));
    });
    return retVal;
  }

  private timestampOut(visitors: Visitor[]): Visitor[] {
    const retVal = [];
    const now = new Date();
    visitors.forEach(visitor => {
      // done by a backend service for complex things like organization affiliation
      retVal.push(new Visitor({ ...visitor, departureTime: now }));
    });
    return retVal;
  }
}
