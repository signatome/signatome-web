import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { startWith, switchMap, debounce, debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeSearchService {
  names = [
    'Afghanistan',
    'Bahamas',
    'Cambodia',
    'Denmark',
    'Ecuador',
    'Falkland Islands',
    'Gabon',
    'Haiti',
    'Iceland',
    'Jamaica',
    'Kazakhstan',
    'Laos',
    'Macau',
    'Namibia',
    'Oman',
    'Pakistan',
    'Qatar',
    'Reunion',
    'Saint Pierre & Miquelon',
    'Taiwan',
    'Uganda',
    'Venezuela',
    'Yemen',
    'Zambia',
    'Zimbabwe'
  ];

  constructor() {}

  search(name: string): Observable<string[]> {
    const search = name === null ? '' : name;
    // http here

    return of(this.names.filter(option => option.toLowerCase().includes(search.toLowerCase())));
  }
}
