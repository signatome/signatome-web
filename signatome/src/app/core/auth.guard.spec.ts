import { TestBed } from '@angular/core/testing';

import { LoginPageGuard } from './auth.guard';

describe('AuthGuard', () => {
  let guard: LoginPageGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LoginPageGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
