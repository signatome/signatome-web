import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginPageGuard implements CanActivate {
  public static readonly REDIRECT_URL_DATA_KEY = 'redirectUrl';
  constructor(private angFireAuth: AngularFireAuth, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const redirectUrl = next.data[LoginPageGuard.REDIRECT_URL_DATA_KEY];

    // if user is already logged in, redirect to redirectURL
    if (this.angFireAuth.auth.currentUser) {
      return this.router.parseUrl(redirectUrl ? redirectUrl : '');
    } else {
      return true;
    }
  }
}
