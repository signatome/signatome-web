import { startWith, map } from 'rxjs/operators';
import { BehaviorSubject, ReplaySubject, Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$: Observable<firebase.User>;
  loggedIn$ = new ReplaySubject<boolean>(1).pipe(startWith(false));
  constructor(private angularFireAuth: AngularFireAuth) {
    this.user$ = this.angularFireAuth.user;
  }
}
