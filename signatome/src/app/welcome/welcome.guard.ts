import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  UrlTree
} from '@angular/router';
import { VisitorLogService } from '../visitor-log.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class WelcomeGuard implements CanActivate {
  constructor(private router: Router, private visitorLogService: VisitorLogService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> {
    return this.visitorLogService.currentVisitors$.pipe(
      map(visitors => {
        if (visitors.length > 0) {
          return true;
        } else {
          return this.router.parseUrl('/sign-in');
        }
      })
    );
  }
}
