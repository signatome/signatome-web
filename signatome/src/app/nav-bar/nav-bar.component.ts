import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  constructor(private fireAuth: AngularFireAuth) {}

  navigation = [
    { link: './about', label: 'About' },
    { link: './features', label: 'Features' },
    { link: './examples', label: 'Examples' },
  ];

  isAuthenticated$: Observable<boolean>;

  ngOnInit(): void {
    this.isAuthenticated$ = this.fireAuth.user.pipe(
      startWith(false),
      map(x => !!x)
    );
  }
}
