import { FormArray } from '@angular/forms';
import { Visitor } from './visitor/visitor.model';
export class VisitorGroupForm {
  visitors = new FormArray([]);
  constructor(visitorGroup: Visitor[]) {
    if (visitorGroup) {
      this.visitors.setValue(visitorGroup);
    }
  }
}
