import { VisitorForm } from './visitor-form.model';
import { Visitor } from './visitor.model';

describe('VisitorForm', () => {
  it('should create an instance', () => {
    expect(new VisitorForm(new Visitor())).toBeTruthy();
  });
});
