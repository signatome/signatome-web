import { Visitor } from './visitor.model';
import { FormControl, Validators } from '@angular/forms';

export class VisitorForm {
  name = new FormControl(this.visitor.name, Validators.required);
  visitPurpose = new FormControl(this.visitor.visitPurpose, Validators.required);
  employeeContact = new FormControl(this.visitor.employeeContact, Validators.required);
  usCitizen = new FormControl(this.visitor.usCitizen);

  // these are not currently represented in the template
  // but exist in order to allow them to be easily added.
  organization = new FormControl(this.visitor.organization);
  email = new FormControl(this.visitor.email);

  constructor(private visitor: Visitor) {}
}
