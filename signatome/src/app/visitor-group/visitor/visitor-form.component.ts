import {
  Component,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output,
  OnInit
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EmployeeSearchService } from 'src/app/employee-search.service';
import { Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-visitor-form',
  templateUrl: './visitor-form.component.html',
  styleUrls: ['./visitor-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VisitorFormComponent implements OnInit {
  @Input() visitorForm: FormGroup;
  @Input() index: number;
  @Input() visitReasons: string[] = [];
  @Output() removeVisitor: EventEmitter<number> = new EventEmitter();

  filteredContacts$: Observable<string[]>;

  remove() {
    this.removeVisitor.emit(this.index);
  }

  constructor(private employeeSearchService: EmployeeSearchService) {}

  ngOnInit(): void {
    // really ought to figure out a type-safe way to reference these controls

    this.filteredContacts$ = this.visitorForm.get('employeeContact').valueChanges.pipe(
      startWith(''),
      switchMap(value => this.employeeSearchService.search(value))
    );
  }
}
