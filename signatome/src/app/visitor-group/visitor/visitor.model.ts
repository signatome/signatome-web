/**
 * Data model that represents a visitor to a business.
 */
export class Visitor {
  // The huge mess of seemingly duplicate text below is the result of wanting to
  // try out the named arguments pattern. It is apparently unsupported by the TS
  // syntax sugar for private fields. Otherwise I wouldn't bother explicitly declaring
  // and assigning the fields.
  // Despite this inconvenience, I've kept it because having named args is

  name: string;
  usCitizen: boolean;
  arrivalTime?: Date;
  departureTime?: Date;
  organization?: string;
  email?: string;
  visitPurpose?: string;
  employeeContact?: string;
  constructor({
    name = '',
    usCitizen = true,
    arrivalTime,
    departureTime,
    organization,
    email,
    visitPurpose,
    employeeContact
  }: {
    name?: string;
    usCitizen?: boolean;
    arrivalTime?: Date;
    departureTime?: Date;
    organization?: string;
    email?: string;
    visitPurpose?: string;
    employeeContact?: string;
  } = {}) {
    this.name = name;
    this.usCitizen = usCitizen;
    this.arrivalTime = arrivalTime;
    this.departureTime = departureTime;
    this.organization = organization;
    this.email = email;
    this.visitPurpose = visitPurpose;
    this.employeeContact = employeeContact;
  }
}
