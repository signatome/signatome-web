import { visitReasons } from './../visit-reasons';
import { VisitorLogService } from './../visitor-log.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { SignInFormService } from './sign-in-form.service';
import { map, filter } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-visitor-group-form',
  templateUrl: './visitor-group-form.component.html',
  styleUrls: ['./visitor-group-form.component.scss']
})
export class VisitorGroupFormComponent implements OnInit {
  reasons = visitReasons;

  visitorsPresent$ = this.visitorLogService.hasVisitors$;

  groupForm$ = this.signinFormService.signInForm$.pipe(
    filter(fg => fg.contains('visitors')),
    map((visitorGroupFormGroup: FormGroup) => ({
      visitorGroupForm: visitorGroupFormGroup,
      visitorFormArray: visitorGroupFormGroup.get('visitors') as FormArray
    }))
  );

  constructor(
    private signinFormService: SignInFormService,
    private visitorLogService: VisitorLogService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.signinFormService.resetForm();
  }

  addVisitor() {
    this.signinFormService.addVisitor();
  }

  removeVisitor(index: number) {
    this.signinFormService.removeVisitor(index);
  }

  submit(value) {
    console.log(value);
    this.visitorLogService.signIn(value.visitors);
    // show confirmation screen
    this.router.navigate(['/confirmation/in/0']);
  }
}
