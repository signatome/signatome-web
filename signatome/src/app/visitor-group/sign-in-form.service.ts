import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Visitor } from './visitor/visitor.model';
import { VisitorForm } from './visitor/visitor-form.model';
import { VisitorGroupForm } from './visitor-group-form.model';

@Injectable({ providedIn: 'root' })
export class SignInFormService {
  // This behavior subject is persisting state because it is longed lived.
  // it should probably get its data from the Visitor log.
  // as a temporary measure, it will get cleared out on form submittal.
  private signInForm: BehaviorSubject<FormGroup | undefined> = new BehaviorSubject(
    this.fb.group(new VisitorGroupForm([]))
  );

  signInForm$: Observable<FormGroup> = this.signInForm.asObservable();

  constructor(private fb: FormBuilder) {}

  addVisitor() {
    const currentSignInForm = this.signInForm.getValue();
    const currentVisitors = currentSignInForm.get('visitors') as FormArray;
    const lastVisitorControl = currentVisitors.at(currentVisitors.controls.length - 1);

    const visitor: Visitor = lastVisitorControl
      ? new Visitor({ ...lastVisitorControl.value, name: '' })
      : new Visitor({ name: 'test', visitPurpose: 'bob', employeeContact: 'test' });

    currentVisitors.push(this.fb.group(new VisitorForm(visitor)));

    this.signInForm.next(currentSignInForm);
  }

  removeVisitor(i: number) {
    const currentSignInForm = this.signInForm.getValue();
    const currentVisitors = currentSignInForm.get('visitors') as FormArray;

    currentVisitors.removeAt(i);
    this.signInForm.next(currentSignInForm);
  }

  resetForm() {
    const currentSignInForm = this.signInForm.getValue();
    const currentVisitors = currentSignInForm.get('visitors') as FormArray;

    // angular 8 may introduce a clear() method. check back later.
    while (currentVisitors.length) {
      currentVisitors.removeAt(currentVisitors.length - 1);
    }

    currentVisitors.push(this.fb.group(new VisitorForm(new Visitor())));

    this.signInForm.next(currentSignInForm);
  }
}
