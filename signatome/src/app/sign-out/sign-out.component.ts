import { Visitor } from './../visitor-group/visitor/visitor.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisitorLogService } from '../visitor-log.service';
import { Observable } from 'rxjs';
import { MatSelectionListChange } from '@angular/material/list';
import { Router } from '@angular/router';
import { tap, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.scss'],
})
export class SignOutComponent implements OnInit, OnDestroy {
  redirectURL = '/welcome';
  allVal = 'SELECT_ALL';
  visitors$: Observable<Visitor[]> = this.visitorLogService.currentVisitors$.pipe(
    filter(x => !!x),
    tap(visitors => {
      if (visitors.length === 0) {
        this.router.navigate([this.redirectURL]);
      }
    }),
    // if we no longer have visitors, don't execute any other steps.
    filter(visitors => visitors.length > 0),
    map((unsorted: Visitor[]) => {
      return unsorted.sort((a: Visitor, b: Visitor) => {
        const timeA = a.arrivalTime ? a.arrivalTime.getTime() : -1;
        const timeB = b.arrivalTime ? b.arrivalTime.getTime() : -1;

        // earlier first
        return timeA - timeB;
      });
    }),
    // auto selects the first in the list
    tap((sorted: Visitor[]) => this.selectedVisitorNames.push(sorted[0].name))
  );

  selectedVisitorNames: string[] = [];
  redirectTimer;

  constructor(private visitorLogService: VisitorLogService, private router: Router) {}

  ngOnInit() {
    // need to design a configuration service for stuff like timeouts
    // maybe just a giant json file.
    console.log('Redirecting to home in 1m');
    this.redirectTimer = setTimeout(() => {
      this.router.navigate([this.redirectURL]);
    }, 60_000);
  }

  ngOnDestroy(): void {
    clearTimeout(this.redirectTimer);
  }

  selectionChange(event: MatSelectionListChange) {
    if (this.allVal === event.option.value) {
      if (event.option.selected) {
        event.source.selectAll();
      } else {
        event.source.deselectAll();
      }
    }
  }

  onSignOut() {
    // console.log('signing out: ', this.selectedVisitorNames);

    this.visitorLogService.signOutVisitors(this.selectedVisitorNames);
    console.log('Signed out visitors: ', this.visitorLogService.signedOut);
    this.router.navigate(['/confirmation/out/0']);
  }
}
