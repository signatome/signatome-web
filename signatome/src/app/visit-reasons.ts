export const visitReasons: string[] = [
  'Meeting',
  'Personal Visit',
  'Cleaning',
  'Maintenance',
  'Interview',
  'Contracting',
  'Other'
];
