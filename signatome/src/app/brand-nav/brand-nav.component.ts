import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-brand-nav',
  templateUrl: './brand-nav.component.html',
  styleUrls: ['./brand-nav.component.scss'],
})
export class BrandNavComponent implements OnInit {
  @Input() name = 'Brand Name';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('quill-pen', sanitizer.bypassSecurityTrustResourceUrl('src/assets/images/quillSvg.svg'));
  }

  ngOnInit(): void {}
}
