import { LoginPageGuard } from './core/auth.guard';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginTestComponent } from './login-test/login-test.component';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';
import { VisitorGroupFormComponent } from './visitor-group/visitor-group-form.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { WelcomeGuard } from './welcome/welcome.guard';
import { AppComponent } from './app.component';
import { LoggedInGuard } from 'ngx-auth-firebaseui';

const routes: Routes = [
  {
    path: 'logged-in',
    component: LoginTestComponent,
    canActivate: [LoggedInGuard],
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
    // canActivate: [LoginPageGuard],
  },
  { path: 'sign-in', component: VisitorGroupFormComponent },
  {
    path: 'sign-out',
    component: SignOutComponent,
    // canActivate: [WelcomeGuard],
  },
  { path: 'confirmation/:msg/:ms', component: ConfirmationComponent },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'about',
        pathMatch: 'full',
      },
      {
        path: 'about',
        component: AboutComponent,
      },
      {
        path: 'features',
        component: AboutComponent,
      },
      {
        path: 'examples',
        component: AboutComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginPageGuard],
        data: { [LoginPageGuard.REDIRECT_URL_DATA_KEY]: '/home' },
      },
    ],
  },
  // {
  //   path: 'login',
  //   outlet: 'publicContent',
  //   // component: LoginComponent,
  //   component: WelcomeComponent,
  // },
  { path: '', redirectTo: 'home', pathMatch: 'full' },

  // { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [WelcomeGuard],
})
export class AppRoutingModule {}
