import { RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { BrandNavComponent } from './brand-nav/brand-nav.component';
import { LoginTestComponent } from './login-test/login-test.component';
import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VisitorFormComponent } from './visitor-group/visitor/visitor-form.component';
import { VisitorGroupFormComponent } from './visitor-group/visitor-group-form.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { FlexLayoutModule } from '@angular/flex-layout';
import * as firebaseConfig from '.firebase-config.json';
import { HomeComponent } from './home/home.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    // UserSignInComponent,
    VisitorGroupFormComponent,
    VisitorFormComponent,
    SignOutComponent,
    ConfirmationComponent,
    LoginComponent,
    LoginTestComponent,
    HomeComponent,
    BrandNavComponent,
    NavBarComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireAuthModule,

    BrowserAnimationsModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FlexLayoutModule,
    NgxAuthFirebaseUIModule.forRoot(
      {
        apiKey: 'AIzaSyCrbpbpBXbg-mIe_TpfYcbpjxkube42Slc',
        authDomain: 'signatome.firebaseapp.com',
        databaseURL: 'https://signatome.firebaseio.com',
        projectId: 'signatome',
        storageBucket: 'signatome.appspot.com',
        messagingSenderId: '474360845006',
        appId: '1:474360845006:web:2b77bfa0800e9c4ce5b69f',
        measurementId: 'G-5JZZTSEFXF',
      },
      () => 'Signatome',
      {
        // enableFirestoreSync: true, // enable/disable autosync users with firestore
        toastMessageOnAuthSuccess: true, // whether to open/show a snackbar message on auth success - default : true
        toastMessageOnAuthError: true, // whether to open/show a snackbar message on auth error - default : true
        authGuardFallbackURL: 'home', // url for unauthenticated users - to use in combination with canActivate feature on a route
        authGuardLoggedInURL: 'welcome', // url for authenticated users - to use in combination with canActivate feature on a route
        passwordMaxLength: 60, // `min/max` input parameters in components should be within this range.
        passwordMinLength: 8, // Password length min/max in forms independently of each componenet min/max.
        // Same as password but for the name
        nameMaxLength: 50,
        nameMinLength: 2,
        // If set, sign-in/up form is not available until email has been verified.
        // Plus protected routes are still protected even though user is connected.
        guardProtectedRoutesUntilEmailIsVerified: false,
        enableEmailVerification: false, // default: true,
      }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
