import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  defaultDuration = 1_500;
  route$: Observable<{ message: string; duration: number }>;

  redirect = () => this.router.navigate(['']);

  ngOnInit() {
    this.route$ = this.activatedRoute.paramMap.pipe(
      map(params => {
        return {
          message: params.get('msg'),
          duration: +params.get('ms')
        };
      }),
      tap(vm => {
        setTimeout(
          this.redirect,
          vm.duration ? vm.duration : this.defaultDuration
        );
      })
    );
  }
}
