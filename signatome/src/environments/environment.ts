// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCrbpbpBXbg-mIe_TpfYcbpjxkube42Slc',
    authDomain: 'signatome.firebaseapp.com',
    databaseURL: 'https://signatome.firebaseio.com',
    projectId: 'signatome',
    storageBucket: 'signatome.appspot.com',
    messagingSenderId: '474360845006',
    appId: '1:474360845006:web:2b77bfa0800e9c4ce5b69f',
    measurementId: 'G-5JZZTSEFXF',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
